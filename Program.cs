﻿using System;

namespace week_5_exercise_26
{
    class Student
    {
        private string Name;
        private int ID;
        public string newName
        {
            get {return Name;}
            set {Name = value;}
        }

        public Student(string _name, int _id)
        {
            Name = _name;
            ID = _id;
        }

        public void Roster()
        {
            Console.WriteLine($"ID '{ID}' belongs to Student: {Name}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var Stu1 = new Student("Mark",20);

            Stu1.Roster();

            Console.WriteLine();
            Console.WriteLine("Enter a new name for the student.");
            Stu1.newName = Console.ReadLine();

            Stu1.Roster();
        }
    }
}
